# VectorGamePatterns

## Introduction
This repository contains the Unity Game Patterns project created by VectorGameDev for educational purposes. The project includes implementations of various design patterns.

## Getting Started
To clone this repository and begin using the project, follow these steps:

```bash
cd existing_repo
git remote add origin https://gitlab.com/Dredro/vectorgamepatterns.git
git branch -M main
git push -uf origin main
```

## Patterns Included
The main branch of this repository contains implementations of the following design patterns:
- State Pattern
- Observer Pattern

## Branches
In addition to the main branch, there may be other branches available for testing individual patterns.

## License
This project is licensed under the GNU General Public License. For more information, please see the [LICENSE](https://www.gnu.org/licenses/gpl-3.0.html) file.

## Project Status
This project is currently in the creation phase.
