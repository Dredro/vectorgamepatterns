using UnityEngine;

namespace ObserverPattern
{
    /// <summary>
    /// Interface for objects that observe changes in subjects.
    /// </summary>
    public interface IObserver
    {
        /// <summary>
        /// Called when the subject notifies observers.
        /// </summary>
        /// <param name="data">Give any data</param>
        void OnNotify(System.Object data);
    }
}