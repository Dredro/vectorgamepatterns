using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace ObserverPattern
{
    /// <summary>
    /// Base class representing the subject in the Observer pattern.
    /// </summary>
    public class Subject : MonoBehaviour
    {
        private List<IObserver> _observers = new();

        /// <summary>
        /// Subscribes an observer to this subject.
        /// </summary>
        /// <param name="observer">The observer to subscribe.</param>
        public void Subscribe(IObserver observer)
        {
            if (!_observers.Contains(observer))
                _observers.Add(observer);
        }

        /// <summary>
        /// Unsubscribes an observer from this subject.
        /// </summary>
        /// <param name="observer">The observer to unsubscribe.</param>
        public void UnSubscribe(IObserver observer)
        {
            if (_observers.Contains(observer))
                _observers.Remove(observer);
        }

        /// <summary>
        /// Notifies all subscribed observers.
        /// </summary>
        protected void Notify(System.Object data)
        {
           _observers.ForEach(_observer =>
           {
               _observer.OnNotify(data);
           });
        }
    }
}