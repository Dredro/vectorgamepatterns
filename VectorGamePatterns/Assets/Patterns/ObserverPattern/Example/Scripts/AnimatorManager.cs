using System;
using System.Collections;
using System.Collections.Generic;
using ObserverPattern;
using UnityEngine;
using UnityEngine.Serialization;

public class AnimatorManager : MonoBehaviour,IObserver
{
    [SerializeField] private Player player;

    private void OnEnable()
    {
        player.Subscribe(this); // SUBSCRIBE PLAYER
    }

    private void OnDisable()
    {
        player.UnSubscribe(this); // SUBSCRIBE PLAYER
    }
    
    public void OnNotify(object data)
    {
        Debug.Log("PlayerAnimation play damage = "+(float)data);
    }
}
