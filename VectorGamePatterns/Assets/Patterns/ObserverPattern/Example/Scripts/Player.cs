using System;
using System.IO;
using System.Reflection;
using ObserverPattern;
using UnityEngine;
using Object = UnityEngine.Object;

public class Player : Subject //INCLUDE SUBJECT !!
{
    public void OnHit(float damage)
    {
       Notify(damage); 
    }
}
