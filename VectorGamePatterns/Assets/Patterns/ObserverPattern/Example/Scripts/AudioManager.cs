using System;
using System.Collections;
using System.Collections.Generic;
using ObserverPattern;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

public class AudioManager : MonoBehaviour,IObserver
{
    [SerializeField] private Player player;
    private void OnEnable()
    {
        player.Subscribe(this); // SUBSCRIBE PLAYER
    }

    private void OnDisable()
    {
        player.UnSubscribe(this); // SUBSCRIBE PLAYER
    }
    

    public void OnNotify(object data) // CALLED WHEN SUBJECT NOTIFY()
    {
        float damage = (float)data; 
        Debug.Log("Playing sound !!! hit sound..damage = "+ damage);
    }
}
