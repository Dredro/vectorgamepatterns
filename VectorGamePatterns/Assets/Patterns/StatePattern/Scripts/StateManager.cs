using UnityEngine;

namespace StatePattern
{
    /// <summary>
    /// Base class for managing states.
    /// </summary>
    public abstract class StateManager : MonoBehaviour
    {
        /// <summary>
        /// The current state of the state manager.
        /// </summary>
        protected IState CurrentState;
        
        /// <summary>
        /// Enables transitioning between states.
        /// </summary>
        /// <param name="state">The new state to switch to.</param>
        public virtual void SwitchState(IState state)
        {
            state.OnExit(this);
            CurrentState = state;
            state.OnEnter(this);
        }
    }
}