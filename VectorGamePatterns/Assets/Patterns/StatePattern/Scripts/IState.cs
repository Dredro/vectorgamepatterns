namespace StatePattern
{
  /// <summary>
  /// Represents an interface for defining state.
  /// </summary>
  public interface IState
  {
      /// <summary>
      /// Invoked when entering the state, passing the <paramref name="stateManager"/>.
      /// </summary>
      /// <param name="stateManager">The state manager.</param>
      void OnEnter(StateManager stateManager);

      /// <summary>
      /// Invoked when exiting the state, passing the <paramref name="stateManager"/>.
      /// </summary>
      /// <param name="stateManager">The state manager.</param>
      void OnExit(StateManager stateManager);

      /// <summary>
      /// Updates the state behavior, passing the <paramref name="stateManager"/>.
      /// </summary>
      /// <param name="stateManager">The state manager.</param>
      void UpdateState(StateManager stateManager);
  }
}

